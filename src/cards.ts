import { v4 as uuidv4 } from 'uuid';
import { EventEmitter } from "node:events"


export interface ISpellCard {
    readonly UUID: string;
    readonly cardName: string;
}

export class spellCard {

}

class attackSpell extends spellCard {

}

class healSpell extends spellCard {

}

class drawCardSpell extends spellCard {

}

class buffSpell extends spellCard {
    
}

export interface IUnitCard {
    attackValue: number;
    healthValue: number;
    isDead: boolean;
    readonly UUID: string;
    readonly cardName: string;
    eventPipe: EventEmitter;

}

export class UnitCard implements IUnitCard  {

    private _attackValue: number;
    private _healthValue: number;
    private _isDead: boolean;
    private _cardName: string;
    private _UUID: string;
    eventPipe: EventEmitter;


    constructor(specificCardName: string = "soldier", attack: number = 1, health: number = 5, dead: boolean = false, eventPipe: EventEmitter) {
        this._cardName = specificCardName;
        this._attackValue = attack;
        this._healthValue = health;
        this._isDead = dead;
        this._UUID = uuidv4();
        this.eventPipe = eventPipe;
    }


    public get attackValue(): number {
        return this._attackValue;
    }
    public set attackValue(value: number) {
        this._attackValue = value;
    }

    public get healthValue(): number {
        return this._healthValue;
    }
    public set healthValue(value: number) {
        this._healthValue = value;
    }

    public get isDead(): boolean {
        return this._isDead;
    }
    public set isDead(value: boolean) {
        this._isDead = value;
    }

    public get cardName(): string {
        return this._cardName;
    }

    public get UUID(): string {
        return this._UUID;
    }

    place() {
        this.eventPipe.emit("placeCard", this)

    }

    death(): boolean {
        return true;
    }

    recieveAttack(attackpoints: number): number {
        return (this.healthValue - attackpoints);
    }


}
