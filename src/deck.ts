import { spellCard, UnitCard } from "./cards";

const MAX_DECK_SIZE = 36;


export class Deck {

    deck = new Array<UnitCard | spellCard>(MAX_DECK_SIZE - 1);

    constructor() {

    }
}

