import { UnitCard } from "./cards";
import { EventEmitter } from "node:events"
import { Player } from "./player";

const MAX_PLAYER_COUNT = 2;


export class FieldManager { //This class's goal is to manage the field of each player
    unitfields = new Map<Player, UnitField>();
    eventPipe: EventEmitter;
    whoPlays = new Array<Player>(MAX_PLAYER_COUNT - 1);
    /**
     * 
     * @param eventPipe this parameter accepts the common use event pipe
     * @param players passes in an array of players
     */
    constructor(eventPipe: EventEmitter, players: Player[]) {
        this.eventPipe = eventPipe;
        if (players.length != 2) {
            throw new Error("players is not equal to 2" + players);
        }
        this.whoPlays = players;
        for (let index = 0; index < this.whoPlays.length; index++) {
            const player = this.whoPlays[index];
            this.unitfields.set(player, new UnitField(player, eventPipe));
        }
    }
}

class UnitField {
    owningPlayer: Player;
    playerField = new Array(4) as Array<UnitCard>;
    eventPipe: EventEmitter;

    constructor(player: Player, eventPipe: EventEmitter) {
        this.owningPlayer = player;
        this.eventPipe = eventPipe;
        this.eventPipe.on("placeCard", this.placeCard)
    }

    placeCard(card: UnitCard) {
        for (let index = this.playerField.length; index > 0; index--) {
            const element = this.playerField[index];
            if (element.UUID) {
                continue;
            } else {
                this.playerField.push(card);
                break;
            }
        }
        throw new Error("unable to place card"); // Will replace this with a function that will cancel action because unable to do and ask the player to try something else

    }

}

